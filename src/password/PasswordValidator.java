package password;

import java.util.regex.Pattern;

/**
 *
 * @author yiyao zhang 991455128
 * Assume spaces are not valid characters for the purpose of calculating length.
 *
 */

public class PasswordValidator {
	
	public static boolean hasValidCaseChars(String password) {
		return password != null && password.matches(".*[A-Z]+.*") && password.matches(".*[a-z]+.*");
	}

    public static boolean isValidLength(String password) {
        if (password == null || password.contains(" ")) {
            return false;
        }
        return password.length() >= 8;
    }

    public static boolean hasValidDigitCount(String password) {
        return Pattern.compile("^.*[0-9].*[0-9].*$").matcher(password).matches();
    }

}
