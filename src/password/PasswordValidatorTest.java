package password;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author yiyao zhang 991455128
 * Assume spaces are not valid characters for the purpose of calculating length.
 *
 */

class PasswordValidatorTest {
	
	@Test
	void testHasValidCaseCharsRegular() {
		assertTrue(PasswordValidator.hasValidCaseChars("fdfdRffff"));
	}
	
	@Test
	void testHasValidCaseCharsBoundaryIn() {
		assertTrue(PasswordValidator.hasValidCaseChars("Aa"));
	}
	
	@Test
	void testHasValidCaseCharsException() {
		assertFalse(PasswordValidator.hasValidCaseChars("12345"));
	}
	
	@Test
	void testHasValidCaseCharsExceptionEmpty() {
		assertFalse(PasswordValidator.hasValidCaseChars(""));
	}
	
	@Test
	void testHasValidCaseCharsExceptionBoundaryOutUpper() {
		assertFalse(PasswordValidator.hasValidCaseChars("A"));
	}
	
	@Test
	void testHasValidCaseCharsExceptionBoundaryOutLower() {
		assertFalse(PasswordValidator.hasValidCaseChars("a"));
	}
	
	
    @Test
    void testIsValidLengthRegular() {
        assertTrue(PasswordValidator.isValidLength("12345678"));
    }

    @Test
    void testIsValidLengthException() {
        assertFalse(PasswordValidator.isValidLength("1234567"));
    }

    @Test
    void testIsValidLengthExceptionSpace() {
        assertFalse(PasswordValidator.isValidLength("1234 5678"));
    }

    @Test
    void testIsValidLengthExceptionNull() {
        assertFalse(PasswordValidator.isValidLength(null));
    }

    @Test
    void testIsValidLengthBoundaryIn() {
        assertTrue(PasswordValidator.isValidLength("12345678"));
    }

    @Test
    void testIsValidLengthBoundaryOut() {
        assertFalse(PasswordValidator.isValidLength("1234567"));
    }

    @Test
    void testHasValidDigitCountRegular() {
        assertTrue(PasswordValidator.hasValidDigitCount("123sfids"));
    }

    @Test
    void testHasValidDigitCountException() {
        assertFalse(PasswordValidator.hasValidDigitCount("sdfoiojs"));
    }

    @Test
    void testHasValidDigitCountBoundaryIn() {
        assertTrue(PasswordValidator.hasValidDigitCount("12abcdef"));
    }

    @Test
    void testHasValidDigitCountBoundaryOut() {
        assertFalse(PasswordValidator.hasValidDigitCount("1abcdefgh"));
    }
}